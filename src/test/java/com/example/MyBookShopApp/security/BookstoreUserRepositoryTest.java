package com.example.MyBookShopApp.security;

import com.example.MyBookShopApp.enitity.user.User;
import com.example.MyBookShopApp.security.repository.BookstoreUserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestPropertySource("/application-test.properties")
class BookstoreUserRepositoryTest
{
    private final BookstoreUserRepository userRepository;

    @Autowired
    public BookstoreUserRepositoryTest(BookstoreUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Test
    public void addNewUser() {
        User user = new User();
        user.setPassword("1234567890");
        user.setPhone("88005553535");
        user.setName("Tester");
        user.setEmail("testing@mail.com");

        assertNotNull(userRepository.save(user));
    }
}