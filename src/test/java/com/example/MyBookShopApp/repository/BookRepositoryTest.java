package com.example.MyBookShopApp.repository;

import com.example.MyBookShopApp.enitity.book.Author;
import com.example.MyBookShopApp.enitity.book.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@TestPropertySource("/application-test.properties")
class BookRepositoryTest
{
    private final BookRepository bookRepository;

    @Autowired
    public BookRepositoryTest(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Test
    void findBooksByAuthorsNameContaining() {
        String token = "Leland Fortoun";
        List<Book> bookListByAuthorFirstName = bookRepository.findBooksByAuthorsNameContaining(token);

        assertNotNull(bookListByAuthorFirstName);
        assertFalse(bookListByAuthorFirstName.isEmpty());

        for(Book book: bookListByAuthorFirstName) {
            for(Author author: book.getAuthors()) {
                assertThat(author.getName()).contains(token);
            }
        }
    }

    @Test
    void findBooksByTitleContaining() {
        String token = "Transcof";
        List<Book> bookListByTitleContaining = bookRepository.findBooksByTitleContaining(token);

        assertNotNull(bookListByTitleContaining);
        assertFalse(bookListByTitleContaining.isEmpty());

        for(Book book: bookListByTitleContaining) {
            assertThat(book.getTitle()).contains(token);
        }
    }

    @Test
    void getBestSellers() {
        List<Book> bestSellersBooks = bookRepository.getBestSellers();

        assertNotNull(bestSellersBooks);
        assertFalse(bestSellersBooks.isEmpty());
        assertThat(bestSellersBooks.size()).isGreaterThan(1);
    }
}