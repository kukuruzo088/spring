insert into book_file_type (id, name, description) values (1, 'PDF', 'int`s pdf' );
insert into book_file_type (id, name, description) values (2, 'EPUB', 'int`s epub');
insert into book_file_type (id, name, description) values (3, 'FB2', 'int`s fb2');

insert into book_file (id, hash, type_id, path, book_id) values (1, 'asdasd1sd12321qadsdasd', 1, '/Transcof.pdf', 1);
insert into book_file (id, hash, type_id, path, book_id) values (2, 'asdasd1sd2321qa12d1asd', 2, '/Transcof.epub', 1);
insert into book_file (id, hash, type_id, path, book_id) values (3, 'asdasdf13r21qadsdasd', 3, '/Transcof.fb2', 1);