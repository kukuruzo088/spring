package com.example.MyBookShopApp.repository;

import com.example.MyBookShopApp.enitity.book.BookRating;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRatingRepository extends JpaRepository<BookRating, Long>
{
}
