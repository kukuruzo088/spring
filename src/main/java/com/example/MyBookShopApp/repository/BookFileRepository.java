package com.example.MyBookShopApp.repository;

import com.example.MyBookShopApp.enitity.book.BookFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookFileRepository extends JpaRepository<BookFile, Long>
{
    public BookFile findBookFileByHash(String hash);
}
