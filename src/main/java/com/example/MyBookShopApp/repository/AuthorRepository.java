package com.example.MyBookShopApp.repository;

import com.example.MyBookShopApp.enitity.book.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public interface AuthorRepository extends JpaRepository<Author, Integer>
{
    Author findAuthorBySlug(String slug);
}
