package com.example.MyBookShopApp.repository;

import com.example.MyBookShopApp.enitity.book.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long>
{
    List<Genre> findGenreByParent(Long parent_id);

    Genre findGenreBySlug(String slug);
}
