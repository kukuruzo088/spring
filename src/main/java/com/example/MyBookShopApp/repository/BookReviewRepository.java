package com.example.MyBookShopApp.repository;

import com.example.MyBookShopApp.enitity.book.BookReview;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookReviewRepository extends JpaRepository<BookReview, Long>
{
}
