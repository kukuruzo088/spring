package com.example.MyBookShopApp.controller;


import com.example.MyBookShopApp.data.dto.BooksPageDto;
import com.example.MyBookShopApp.data.dto.SearchWordDto;
import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.enitity.book.Tag;
import com.example.MyBookShopApp.exeption.EmptySearchException;
import com.example.MyBookShopApp.service.book.BookService;
import com.example.MyBookShopApp.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/")
public class MainPageController implements ProjectController
{
    @Value("${book.slider.size}")
    private Integer pageSize;

    private final BookService service;
    private final TagService tagService;

    @Autowired
    public MainPageController(BookService service, TagService tagService) {
        this.tagService = tagService;
        this.service = service;
    }

    @ModelAttribute("recommendedBooks")
    public List<Book> recommendedBooks() {
        return service.getPageOfRecommendedBooks(0, pageSize).getContent();
    }

    @ModelAttribute("recentBooks")
    public List<Book> recentBooks() {
        return service.getPageOfRecentBooks(0, pageSize).getContent();
    }

    @ModelAttribute("popularBooks")
    public List<Book> popularBooks() {
        return service.getPageOfPopularBooks(0, pageSize).getContent();
    }

    @ModelAttribute("tagsCloud")
    public List<Tag> tagsCloud() {
        return tagService.getTagsWithBooks();
    }

    @ModelAttribute("pageSize")
    public Integer getPageSize() {
        return pageSize;
    }

    @GetMapping
    public String index() {
        return "index";
    }

    @GetMapping(value = {"/search", "/search/{searchWord}"})
    public String getSearchResult(@PathVariable(value = "searchWord", required = false) SearchWordDto searchWordDto, Model model) throws EmptySearchException {
        if(searchWordDto == null) {
            throw new EmptySearchException("Поиск по null невозможен");
        }
        model.addAttribute("searchWordDto", searchWordDto);
        model.addAttribute("searchResults",
                service.getPageOfGoogleBooksApiSearchResult(searchWordDto.getExample(), 0, pageSize));
        return "/search/index";
    }

    @GetMapping("/search/page/{searchWord}")
    @ResponseBody
    public BooksPageDto getNextSearchPage(
            @RequestParam("offset") Integer offset,
            @RequestParam("limit") Integer limit,
            @PathVariable(value = "searchWord", required = false) SearchWordDto searchWordDto
    ) {
        return new BooksPageDto(service.getPageOfGoogleBooksApiSearchResult(searchWordDto.getExample(), offset, limit));
    }

}