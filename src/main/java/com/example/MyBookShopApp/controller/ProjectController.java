package com.example.MyBookShopApp.controller;

import com.example.MyBookShopApp.data.dto.SearchWordDto;
import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.data.enums.Menu;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface ProjectController {
    @ModelAttribute("menuItems")
    default List<Menu> menuItems(HttpServletRequest request) {
        String uri = request.getRequestURI();
        List<Menu> menuItems = new ArrayList<>(Arrays.asList(
                Menu.MAIN,
                Menu.GENRES,
                Menu.RECENT,
                Menu.POPULAR,
                Menu.AUTHORS));

        menuItems.forEach(item -> item.active = item.link.equals(uri));
        return menuItems;
    }

    @ModelAttribute("searchWordDto")
    default SearchWordDto searchWordDto() {
        return new SearchWordDto();
    }

    @ModelAttribute("searchResults")
    default List<Book> searchResults() {
        return new ArrayList<>();
    }
}
