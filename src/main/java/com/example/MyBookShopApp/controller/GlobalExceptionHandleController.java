package com.example.MyBookShopApp.controller;

import com.example.MyBookShopApp.data.ApiResponse;
import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.exeption.BookstoreApiWrongParameterException;
import com.example.MyBookShopApp.exeption.EmptySearchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@ControllerAdvice
public class GlobalExceptionHandleController
{
    @ExceptionHandler(EmptySearchException.class)
    public String handleEmptySearchException(EmptySearchException exception, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("searchError", exception);
        return "redirect:/";
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ApiResponse<Book>> handleMissingServletRequestParameterException(Exception exception) {
        return new ResponseEntity<>(new ApiResponse<Book>(
                HttpStatus.BAD_REQUEST,
                "Missing required parameters",
                exception
        ), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BookstoreApiWrongParameterException.class)
    public ResponseEntity<ApiResponse<Book>> handleBookstoreApiWrongParameterException(Exception exception) {
        return new ResponseEntity<>(new ApiResponse<Book>(
                HttpStatus.BAD_REQUEST,
                "Bad parameter value...",
                exception
        ), HttpStatus.BAD_REQUEST);
    }
}
