package com.example.MyBookShopApp.controller.book;

import com.example.MyBookShopApp.controller.ProjectController;
import com.example.MyBookShopApp.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("genres")
public class GenresController implements ProjectController
{
    private final GenreService service;

    @Autowired
    public GenresController(GenreService service) {
        this.service = service;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("genres", service.findAllParent());
        return "/genres/index";
    }

    @GetMapping("/{slug}")
    public String detail(Model model, @PathVariable("slug") String slug) {
        model.addAttribute("genre", service.findBySlug(slug));
        return "/genres/slug";
    }
}
