package com.example.MyBookShopApp.controller.book;

import com.example.MyBookShopApp.data.ApiResponse;
import com.example.MyBookShopApp.data.dto.BooksPageDto;
import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.enitity.book.BookRating;
import com.example.MyBookShopApp.enitity.book.BookReview;
import com.example.MyBookShopApp.exeption.BookstoreApiWrongParameterException;
import com.example.MyBookShopApp.service.book.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/books")
@Api(description = "book data api")
@Validated
public class BookRestApiController
{
    private final BookService service;

    @Autowired
    public BookRestApiController(BookService service) {
        this.service = service;
    }

    @GetMapping("/by-author")
    @ApiOperation("operation to get book list of bookshop by passed author first name")
    public ResponseEntity<List<Book>> bookByAuthor(@RequestParam("author") String authorName) {
        return ResponseEntity.ok(service.getBookByAuthor(authorName));
    }

    @GetMapping("/by-title")
    @ApiOperation("operation to get books by book title")
    public ResponseEntity<ApiResponse<Book>> booksByTitle(@RequestParam("title") String bookTitle) throws BookstoreApiWrongParameterException {
        ApiResponse<Book> response = new ApiResponse<>();
        List<Book> data = service.getBooksByTitle(bookTitle);
        response.setDebugMessage("successful request");
        response.setMessage("data size: " + data.size() + " elements");
        response.setStatus(HttpStatus.OK);
        response.setTimeStamp(LocalDateTime.now());
        response.setData(data);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/by-price-range")
    @ApiOperation("operation to get books by price range from min price to max price")
    public ResponseEntity<List<Book>> priceRangeBooks(@RequestParam("min") Integer min, @RequestParam("max") Integer max) {
        return ResponseEntity.ok(service.getBooksWithPriceBetween(min, max));
    }

    @GetMapping("/with-max-discount")
    @ApiOperation("get list of book with max discount")
    public ResponseEntity<List<Book>> maxDiscountBooks() {
        return ResponseEntity.ok(service.getBookWithMaxPrice());
    }

    @GetMapping("/bestsellers")
    @ApiOperation("get bestsellers books (witch is_bestseller = 1)")
    public ResponseEntity<List<Book>> bestSellersBooks() {
        return ResponseEntity.ok(service.getBestsellers());
    }

    @GetMapping("/recommended")
    @ApiOperation("get list of recommended book")
    public BooksPageDto getRecommendedBooksPage(@RequestParam("offset") Integer offset, @RequestParam("limit") Integer limit) {
        return new BooksPageDto(service.getPageOfRecommendedBooks(offset, limit).getContent());
    }

    @GetMapping("/recent")
    @ApiOperation("get list of recent book")
    public BooksPageDto getRecentBooksPage(
            @RequestParam("offset") Integer offset,
            @RequestParam("limit") Integer limit,
            @RequestParam(value = "from", required = false) String strFrom,
            @RequestParam(value = "to", required = false) String strTo
    ) throws ParseException {
        if(strFrom != null && strTo != null) {
            Date from = new SimpleDateFormat("dd.MM.yyyy").parse(strFrom);
            Date to = new SimpleDateFormat("dd.MM.yyyy").parse(strTo);
            return new BooksPageDto(service.getPageOfRecentBooks(from, to, offset, limit).getContent());
        }
        return new BooksPageDto(service.getPageOfRecentBooks(offset, limit).getContent());
    }

    @GetMapping("/popular")
    @ApiOperation("get list of popular book")
    public BooksPageDto getPopularBooksPage(@RequestParam("offset") Integer offset, @RequestParam("limit") Integer limit) {
        return new BooksPageDto(service.getPageOfPopularBooks(offset, limit).getContent());
    }

    @PostMapping("/changeBookStatus/{slug}")
    public ResponseEntity handleChangeBookStatus(
            @PathVariable("slug") String slug,
            HttpServletRequest request,
            HttpServletResponse response, Model model) {
        Book book = service.getBookBySlug(slug);
        String status = request.getParameter("status");
        if ("CART".equals(status)) {
            service.addToBasket(request, response, book);
        }
        if ("KEPT".equals(status)) {
            service.addToKept(request, response, book);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/changeBookStatus/cart/remove/{slug}")
    public ResponseEntity<HttpServletResponse> handleRemoveBookFromCartRequest(
            @PathVariable("slug") String slug,
            @CookieValue(name = "cartContents", required = false) String cartContents,
            HttpServletResponse response, Model model) {
        if(cartContents != null && !cartContents.equals("")) {
            ArrayList<String> cookieBooks = new ArrayList<>(Arrays.asList(cartContents.split("/")));
            cookieBooks.remove(slug);
            Cookie cookie = new Cookie("cartContents", String.join("/", cookieBooks));
            cookie.setPath("/");
            response.addCookie(cookie);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/review/{book}")
    public ResponseEntity<ApiResponse<BookReview>> addReview(
            @RequestBody BookReview review,
            @PathVariable("book") Book book,
            Authentication authentication
    ) {
        review = service.addReview(book, review, authentication);
        ApiResponse<BookReview> response = new ApiResponse<>();
        response.setMessage("review added successfully");
        response.setStatus(HttpStatus.OK);
        response.setTimeStamp(LocalDateTime.now());
        response.setItem(review);
        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiResponse<BookReview>> handleConstraintViolationException(Exception exception) {
        return new ResponseEntity<>(new ApiResponse<>(
                HttpStatus.BAD_REQUEST,
                exception.getMessage(),
                exception
        ), HttpStatus.BAD_REQUEST);
    }

    @PostMapping(value = "/add-rating/{book}")
    public ResponseEntity<ApiResponse<BookRating>> addScore(
            @PathVariable Book book,
            @RequestBody BookRating rating,
            Authentication authentication
    ) {
        rating = service.addScore(book, rating, authentication);
        ApiResponse<BookRating> response = new ApiResponse<>();
        response.setMessage("rating added successfully");
        response.setStatus(HttpStatus.OK);
        response.setTimeStamp(LocalDateTime.now());
        response.setItem(rating);
        return ResponseEntity.ok(response);
    }
}
