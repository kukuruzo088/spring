package com.example.MyBookShopApp.controller.book;

import com.example.MyBookShopApp.controller.ProjectController;
import com.example.MyBookShopApp.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class TagController implements ProjectController
{
    @Value("${book.list.size}")
    private Integer pageSize;

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @ModelAttribute("pageSize")
    public Integer getPageSize() {
        return pageSize;
    }

    @GetMapping("/tags/{slug}")
    public String index(@PathVariable("slug") String slug, Model model) {
        model.addAttribute("tag", tagService.getTagBySlug(slug));
        return "tags/index";
    }
}
