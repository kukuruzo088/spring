package com.example.MyBookShopApp.controller.book;

import com.example.MyBookShopApp.controller.ProjectController;
import com.example.MyBookShopApp.data.PaymentService;
import com.example.MyBookShopApp.data.ResourceStorage;
import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.enitity.book.BookFile;
import com.example.MyBookShopApp.repository.BookFileRepository;
import com.example.MyBookShopApp.repository.BookRepository;
import com.example.MyBookShopApp.service.book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/books")
public class BookController implements ProjectController
{
    @Value("${book.list.size}")
    private Integer pageSize;

    private final BookService service;
    private final ResourceStorage storage;
    private final BookRepository repository;
    private final PaymentService paymentService;
    private final BookFileRepository fileRepository;

    @Autowired
    public BookController(
            BookService service,
            ResourceStorage storage,
            BookRepository repository,
            PaymentService paymentService, BookFileRepository fileRepository
    ) {
        this.service = service;
        this.storage = storage;
        this.repository = repository;
        this.paymentService = paymentService;
        this.fileRepository = fileRepository;
    }

    @ModelAttribute("pageSize")
    public Integer getPageSize() {
        return pageSize;
    }

    @GetMapping("/recent")
    public String recent(Model model) {
        model.addAttribute("recentBooks", service.getPageOfRecentBooks(0, pageSize).getContent());
        return "/books/recent";
    }

    @GetMapping("/popular")
    public String popular(Model model) {
        model.addAttribute("popularBooks", service.getPageOfPopularBooks(0, pageSize).getContent());
        return "/books/popular";
    }

    @GetMapping("/{slug}")
    public String detail(@PathVariable("slug") String slug, Model model) {
        model.addAttribute("book", service.getBookBySlug(slug));
        return "/books/slug";
    }

    @PostMapping("/{slug}/img/save")
    public String saveNewBookImage(
            @RequestParam("file") MultipartFile file,
            @PathVariable("slug") String slug
    ) throws IOException {
        String savePath = storage.saveNewBookImage(file, slug);
        Book book = service.getBookBySlug(slug);
        book.setImage(savePath);
        repository.save(book);
        return ("redirect:/books/" + slug);
    }

    @GetMapping("/download/{hash}")
    public ResponseEntity<ByteArrayResource> download(@PathVariable("hash") String hash) throws IOException {
        BookFile file = fileRepository.findBookFileByHash(hash);
        storage.setFile(file);

        Path path = storage.getBookFilePath(hash);
        MediaType mediaType = storage.getBookFileMime(hash);
        byte[] data = storage.getBookFileByteArray(hash);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename" + path.getFileName().toString())
                .contentType(mediaType)
                .body(new ByteArrayResource(data));
    }

    @GetMapping("/pay")
    public RedirectView handlePay(HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException {
        List<Book> bookCart =  service.getBookInBasket(request, response);
        String paymentUrl = paymentService.getPaymentUrl(bookCart);
        return new RedirectView(paymentUrl);
    }
}
