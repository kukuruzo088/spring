package com.example.MyBookShopApp.controller.author;

import com.example.MyBookShopApp.enitity.book.Author;
import com.example.MyBookShopApp.service.AuthorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/authors")
@Api(description = "authors data")
public class AuthorRestController
{
    private final AuthorService authorService;

    @Autowired
    public AuthorRestController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @ModelAttribute("authorsAlphabet")
    public Map<String, List<Author>> authorsAlphabet()
    {
        return authorService.getAuthorsAlphabet();
    }

    @ApiOperation("method to get map of authors")
    @GetMapping
    @ResponseBody
    public Map<String, List<Author>> authors() {
        return authorService.getAuthorsAlphabet();
    }
}
