package com.example.MyBookShopApp.controller.author;

import com.example.MyBookShopApp.controller.ProjectController;
import com.example.MyBookShopApp.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/authors")
public class AuthorsController implements ProjectController
{
    private final AuthorService authorService;

    @Autowired
    public AuthorsController(AuthorService authorService) {
        this.authorService = authorService;
    }

    @GetMapping
    public String index(Model model) {
        model.addAttribute("authorsAlphabet", authorService.getAuthorsAlphabet());
        return "/authors/index";
    }

    @GetMapping("/{slug}")
    public String detail(@PathVariable(value = "slug") String slug, Model model) {
        model.addAttribute("author", authorService.findBySlug(slug));
        return "/authors/slug";
    }
}
