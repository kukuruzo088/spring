package com.example.MyBookShopApp.controller.basket;

import com.example.MyBookShopApp.controller.ProjectController;
import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.service.book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
public class StatusBookController implements ProjectController
{
    private final BookService service;

    @Autowired
    public StatusBookController(BookService service) {
        this.service = service;
    }

    @GetMapping("/cart")
    public String cart(HttpServletRequest request, HttpServletResponse response, Model model) {
        List<Book> bookCart =  service.getBookInBasket(request, response);
        model.addAttribute("isCartEmpty", bookCart.isEmpty());
        model.addAttribute("bookCart", bookCart);
        return "cart";
    }

    @GetMapping("/postponed")
    public String postponed(HttpServletRequest request, HttpServletResponse response, Model model) {
        model.addAttribute("bookKept", service.getBookInKept(request, response));
        return "postponed";
    }
}
