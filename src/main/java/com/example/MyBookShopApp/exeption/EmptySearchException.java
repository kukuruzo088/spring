package com.example.MyBookShopApp.exeption;

public class EmptySearchException extends Exception {
    public EmptySearchException(String message) {
        super(message);
    }
}
