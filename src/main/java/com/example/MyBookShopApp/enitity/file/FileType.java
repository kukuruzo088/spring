package com.example.MyBookShopApp.enitity.file;

import com.example.MyBookShopApp.data.enums.FIleTypeName;

import javax.persistence.*;

@Entity
@Table(name = "book_file_type", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class FileType
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Enumerated(EnumType.STRING)
    private FIleTypeName name;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public FIleTypeName getName() {
        return name;
    }

    public void setName(FIleTypeName name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
