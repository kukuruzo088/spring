package com.example.MyBookShopApp.enitity.transaction;

import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.enitity.user.User;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "balance_transaction")
public class BalanceTransaction
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;
    private Date time;
    private Integer value;
    @OneToOne
    @JoinColumn(name = "book_id")
    private Book book;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
