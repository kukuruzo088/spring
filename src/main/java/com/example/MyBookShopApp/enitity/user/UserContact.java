package com.example.MyBookShopApp.enitity.user;

import com.example.MyBookShopApp.data.enums.ContactType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_contact")
public class UserContact
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    private Integer approved;
    private String code;

    @Enumerated(EnumType.STRING)
    private ContactType type;
    private Integer code_trials;
    private Date code_time;
    private String contact;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer isApproved() {
        return approved;
    }

    public void setApproved(Integer approved) {
        this.approved = approved;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public Integer getCode_trials() {
        return code_trials;
    }

    public void setCode_trials(Integer code_trials) {
        this.code_trials = code_trials;
    }

    public Date getCode_time() {
        return code_time;
    }

    public void setCode_time(Date code_time) {
        this.code_time = code_time;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
