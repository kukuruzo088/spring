package com.example.MyBookShopApp.enitity.book;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "book_review_like")
public class BookReviewLike
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "review_id", referencedColumnName = "id")
    private BookReview review;
    private Date time;

    private Integer value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BookReview getReview() {
        return review;
    }

    public void setReview(BookReview review) {
        this.review = review;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer isValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
