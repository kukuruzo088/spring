package com.example.MyBookShopApp.enitity.book;

import com.example.MyBookShopApp.data.enums.FIleTypeName;
import com.example.MyBookShopApp.enitity.file.FileType;

import javax.persistence.*;

@Entity
@Table(name = "book_file")
public class BookFile
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String hash;

    @ManyToOne
    @JoinColumn(name = "type_id", referencedColumnName = "id")
    private FileType type;
    private String path;

    @ManyToOne
    @JoinColumn(name = "book_id", referencedColumnName = "id")
    private Book book;

    public String getBookFileExtensionString() {
        return FIleTypeName.getExtensionStringByTypeId(type.getId());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public FileType getType() {
        return type;
    }

    public void setType(FileType type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
