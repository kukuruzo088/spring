package com.example.MyBookShopApp.enitity.book;

import com.example.MyBookShopApp.data.enums.TypesName;

import javax.persistence.*;

@Entity
@Table(name = "book2user_type", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name"),
        @UniqueConstraint(columnNames = "code")
})
public class BookType
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private TypesName code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypesName getCode() {
        return code;
    }

    public void setCode(TypesName code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
