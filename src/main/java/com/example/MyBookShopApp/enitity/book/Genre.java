package com.example.MyBookShopApp.enitity.book;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "genres")
public class Genre
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String slug;

    @ManyToOne
    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    private Genre parent;

    @OneToMany(mappedBy = "parent")
    private List<Genre> children;

    @ManyToMany(mappedBy = "genres")
    private List<Book> books;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public List<Genre> getChildren() {
        return children;
    }

    public void setParent(List<Genre> parent) {
        this.children = parent;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Genre getParent() {
        return parent;
    }

    public void setParent(Genre parent) {
        this.parent = parent;
    }

    public void setChildren(List<Genre> children) {
        this.children = children;
    }
}
