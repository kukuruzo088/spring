package com.example.MyBookShopApp.data;

import com.example.MyBookShopApp.enitity.book.BookFile;
import com.example.MyBookShopApp.repository.BookFileRepository;
import liquibase.util.file.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ResourceStorage
{
    @Value("${upload.path}")
    String uploadPath;

    @Value("${download.path}")
    String downloadPath;

    private final BookFileRepository fileRepository;
    private BookFile file;

    @Autowired
    public ResourceStorage(BookFileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public void setFile(BookFile file) {
        this.file = file;
    }

    public String saveNewBookImage(MultipartFile file, String slug) throws IOException {
        String resourceURI = null;

        if(!file.isEmpty()) {
            if(!new File(uploadPath).exists()) {
                Files.createDirectories(Paths.get(uploadPath));
            }
            String fileName = slug + "." + FilenameUtils.getExtension(file.getOriginalFilename());
            Path path = Paths.get(uploadPath, fileName);
            resourceURI = "/upload/" + fileName;
            file.transferTo(path);
        }
        return resourceURI;
    }

    public Path getBookFilePath(String hash) {
        return Paths.get(file.getPath());
    }

    public MediaType getBookFileMime(String hash) {
        String mimeType = URLConnection.guessContentTypeFromName(Paths.get(file.getPath()).toString());
        if(mimeType == null) {
            return MediaType.APPLICATION_OCTET_STREAM;
        }
        return MediaType.parseMediaType(mimeType);
    }

    public byte[] getBookFileByteArray(String hash) throws IOException {
        Path path = Paths.get(downloadPath, file.getPath());
        return Files.readAllBytes(path);
    }
}
