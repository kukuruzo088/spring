package com.example.MyBookShopApp.data;

import com.example.MyBookShopApp.enitity.book.Book;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class PaymentService {

    @Value("${robokassa.merchant.login}")
    private String robokassaMerchantLogin;

    @Value("${robokassa.pass.first}")
    private String robokassaPassFirst;


    public String getPaymentUrl(List<Book> books) throws NoSuchAlgorithmException {
        int invId = 5;
        double paymentSumTotal = books.stream().mapToDouble(Book::getDiscountedPrice).sum();
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(
                (robokassaMerchantLogin +
                    ":" + paymentSumTotal +
                    ":" + invId +
                    ":" + robokassaPassFirst).getBytes());
        String signatureValues = DatatypeConverter.printHexBinary(messageDigest.digest()).toUpperCase();
        return "http://auth.robokassa.ru/Merchant/Index.aspx"
                + "?MerchantLogin=" + robokassaMerchantLogin
                + "&InvId=" + invId
                + "&Culture=ru"
                + "&Encoding=utf-8"
                + "&OutSum=" + paymentSumTotal
                + "&SignatureValue=" + signatureValues
                + "&isTest=1";
    }
}
