package com.example.MyBookShopApp.data.enums;

public enum TypesName
{
    KEPT,
    CART,
    PAID,
    ARCHIVED
}
