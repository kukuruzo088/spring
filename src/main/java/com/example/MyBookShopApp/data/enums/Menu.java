package com.example.MyBookShopApp.data.enums;

public enum Menu
{
    MAIN("main", "/"),
    GENRES("genres", "/genres"),
    RECENT("recent", "/books/recent"),
    POPULAR("popular", "/books/popular"),
    AUTHORS("authors", "/authors");

    public String type;
    public  String link;
    public boolean active = false;


    Menu(String type, String link) {
        this.type = type;
        this.link = link;
    }
}
