package com.example.MyBookShopApp.data.enums;

public enum FIleTypeName
{
    PDF(".pdf"),
    EPUB(".epub"),
    FB2(".fb2");

    private final String fileExtensionString;

    FIleTypeName(String fileExtensionString) {
        this.fileExtensionString = fileExtensionString;
    }

    public static String getExtensionStringByTypeId(Long type_id) {
        if (type_id == 1) {
            return FIleTypeName.PDF.fileExtensionString;
        } else if (type_id == 2) {
            return FIleTypeName.EPUB.fileExtensionString;
        } else if (type_id == 3) {
            return FIleTypeName.FB2.fileExtensionString;
        }
        return "";
    }
}
