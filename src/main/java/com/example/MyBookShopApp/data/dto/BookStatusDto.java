package com.example.MyBookShopApp.data.dto;

import com.example.MyBookShopApp.enitity.book.Book;

public class BookStatusDto
{
    private String status;
    private Book book;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
