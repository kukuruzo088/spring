package com.example.MyBookShopApp.security.service;

import com.example.MyBookShopApp.enitity.user.User;
import com.example.MyBookShopApp.security.data.BookstoreUserDetails;
import com.example.MyBookShopApp.security.repository.BookstoreUserRepository;
import com.example.MyBookShopApp.security.data.PhoneNumberUserDetails;
import com.example.MyBookShopApp.security.jwt.JWTBlacklist;
import com.example.MyBookShopApp.security.jwt.JWTBlacklistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Optional;

@Service
public class BookstoreUserDetailService implements UserDetailsService, LogoutHandler
{
    private final BookstoreUserRepository bookstoreUserRepository;
    private final JWTBlacklistRepository jwtBlacklistRepository;

    @Autowired
    public BookstoreUserDetailService(BookstoreUserRepository bookstoreUserRepository, JWTBlacklistRepository jwtBlacklistRepository) {
        this.bookstoreUserRepository = bookstoreUserRepository;
        this.jwtBlacklistRepository = jwtBlacklistRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User bookstoreUser;
        UserDetails userDetails;
        if(s.contains("@")) {
            bookstoreUser = bookstoreUserRepository.findBookstoreUserByEmail(s);
            userDetails = new BookstoreUserDetails(bookstoreUser);
        } else {
            bookstoreUser = bookstoreUserRepository.findBookstoreUserByPhone(s);
            userDetails = new PhoneNumberUserDetails(bookstoreUser);
        }
        if(bookstoreUser == null) {
            throw new UsernameNotFoundException("user not found!");
        }
        return userDetails;
    }

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        Optional<Cookie> optionalCookie = Arrays.stream(request.getCookies()).filter(cookie -> "token".equals(cookie.getName()))
                .findFirst();
        if(optionalCookie.isPresent()) {
            String token = optionalCookie.get().getValue();
            JWTBlacklist jwtBlacklist = new JWTBlacklist();
            jwtBlacklist.setToken(token);
            jwtBlacklistRepository.save(jwtBlacklist);
        }
    }
}
