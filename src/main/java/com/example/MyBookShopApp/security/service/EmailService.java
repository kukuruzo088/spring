package com.example.MyBookShopApp.security.service;

import com.example.MyBookShopApp.enitity.SmsCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService
{
    private final SmsService smsService;
    private final JavaMailSender mailSender;

    @Value("${sms.code.livetime}")
    private String smsCodeLiveTime;

    @Value("${appEmail.email}")
    private String from;

    @Autowired
    public EmailService(SmsService smsService, JavaMailSender mailSender) {
        this.smsService = smsService;
        this.mailSender = mailSender;
    }

    public void sendSecretCode(String to) {
        SmsCode smsCode = new SmsCode(smsService.generateCode(), Integer.valueOf(smsCodeLiveTime));
        smsService.saveNewCode(smsCode);

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject("Bookstore email verification");
        message.setText("Verification code is: " + smsCode.getCode());
        mailSender.send(message);
    }
}
