package com.example.MyBookShopApp.security.service;

import com.example.MyBookShopApp.enitity.user.User;
import com.example.MyBookShopApp.security.data.ContactConfirmationResponse;
import com.example.MyBookShopApp.security.data.BookstoreUserDetails;
import com.example.MyBookShopApp.security.data.ContactConfirmationPayload;
import com.example.MyBookShopApp.security.data.RegistrationForm;
import com.example.MyBookShopApp.security.jwt.JWTUtil;
import com.example.MyBookShopApp.security.repository.BookstoreUserRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class BookstoreUserRegister
{
    private final BookstoreUserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final BookstoreUserDetailService detailService;
    private final JWTUtil jwtUtil;

    public BookstoreUserRegister(BookstoreUserRepository repository, PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager, BookstoreUserDetailService detailService, JWTUtil jwtUtil) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.detailService = detailService;
        this.jwtUtil = jwtUtil;
    }

    public User registerUser(RegistrationForm registrationForm) {
        User user = new User();
        User userByEmail = repository.findBookstoreUserByEmail(registrationForm.getEmail());
        User userByPhone = repository.findBookstoreUserByPhone(registrationForm.getPhone());
        if(userByEmail != null && userByPhone != null) {
            user = userByPhone;
        } else {
            user.setEmail(registrationForm.getEmail());
            user.setName(registrationForm.getName());
            user.setPhone(registrationForm.getPhone());
            user.setPassword(passwordEncoder.encode(registrationForm.getPass()));
            user = repository.save(user);
        }
        return user;
    }

    public User getCurrentUser(Authentication authentication) {
        User user = new User();
        Object principal = authentication.getPrincipal();
        if(principal instanceof DefaultOAuth2User) {
            Map<String, Object> attributes = ((DefaultOAuth2User) principal).getAttributes();
            user.setName(attributes.get("name").toString());
            user.setEmail(attributes.get("email").toString());
        } else if(principal instanceof BookstoreUserDetails) {
            BookstoreUserDetails userDetails = (BookstoreUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            user = userDetails.getBookstoreUser();
        }
        return user;
    }

    public ContactConfirmationResponse jwtLogin(ContactConfirmationPayload payload) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(payload.getContact(), payload.getCode()));
        BookstoreUserDetails bookstoreUserDetails = (BookstoreUserDetails) detailService.loadUserByUsername(payload.getContact());
        String jwtToken = jwtUtil.generateToken(bookstoreUserDetails);
        ContactConfirmationResponse response = new ContactConfirmationResponse();
        response.setResult(jwtToken);
        return response;
    }

    public ContactConfirmationResponse jwtLoginByPhoneNumber(ContactConfirmationPayload payload) {
        UserDetails userDetails = detailService.loadUserByUsername(payload.getContact());
        String jwtToken = jwtUtil.generateToken(userDetails);
        ContactConfirmationResponse response = new ContactConfirmationResponse();
        response.setResult(jwtToken);
        return response;
    }
}
