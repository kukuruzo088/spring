package com.example.MyBookShopApp.security.controller;

import com.example.MyBookShopApp.controller.ProjectController;
import com.example.MyBookShopApp.data.ApiResponse;
import com.example.MyBookShopApp.enitity.user.User;
import com.example.MyBookShopApp.security.data.ContactConfirmationPayload;
import com.example.MyBookShopApp.security.data.ContactConfirmationResponse;
import com.example.MyBookShopApp.security.data.RegistrationForm;
import com.example.MyBookShopApp.security.service.BookstoreUserRegister;
import com.example.MyBookShopApp.security.service.EmailService;
import com.example.MyBookShopApp.security.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
public class AuthUserController implements ProjectController
{
    private final BookstoreUserRegister userRegister;
    private final SmsService smsService;
    private final EmailService emailService;

    @Autowired
    public AuthUserController(BookstoreUserRegister userRegister, SmsService smsService, EmailService emailService) {
        this.userRegister = userRegister;
        this.smsService = smsService;
        this.emailService = emailService;
    }

    @GetMapping("/signin")
    public String handleSignIn() {
        return "signin";
    }

    @GetMapping("/signup")
    public String handleSignUp(Model model) {
        model.addAttribute("regForm", new RegistrationForm());
        return "signup";
    }

    @PostMapping("/requestContactConfirmation")
    @ResponseBody
    public ContactConfirmationResponse handleRequestContactConfirmation(@RequestBody ContactConfirmationPayload payload) {
        ContactConfirmationResponse response = new ContactConfirmationResponse();
        response.setResult("true");
        smsService.sendSecretCode(payload.getContact());
        return response;
    }

    @PostMapping("/requestEmailConfirmation")
    @ResponseBody
    public ContactConfirmationResponse handleRequestEmailConfirmation(@RequestBody ContactConfirmationPayload payload) {
        ContactConfirmationResponse response = new ContactConfirmationResponse();
        emailService.sendSecretCode(payload.getContact());
        response.setResult("true");
        return response;
    }

    @PostMapping("/approveContact")
    @ResponseBody
    public ContactConfirmationResponse handleApproveContact(@RequestBody ContactConfirmationPayload payload) {
        ContactConfirmationResponse response = new ContactConfirmationResponse();

        if(smsService.verifyCode(payload.getCode())) {
            response.setResult("true");
        }

        return response;
    }

    @PostMapping("/reg")
    public String register(RegistrationForm registrationForm, Model model) {
        userRegister.registerUser(registrationForm);
        model.addAttribute("regOk", true);
        return "signin";
    }

    @PostMapping("/login")
    @ResponseBody
    public  ContactConfirmationResponse login(@RequestBody ContactConfirmationPayload payload, HttpServletResponse response) {
        ContactConfirmationResponse loginResponse = userRegister.jwtLogin(payload);
        Cookie cookie = new Cookie("token", loginResponse.getResult());
        response.addCookie(cookie);
        return loginResponse;
    }

    @PostMapping("/login-by-phone-number")
    @ResponseBody
    public  ContactConfirmationResponse loginByPhoneNumber(@RequestBody ContactConfirmationPayload payload, HttpServletResponse response) {
        if(!smsService.verifyCode(payload.getCode())) {
            return new ContactConfirmationResponse();
        }
        ContactConfirmationResponse loginResponse = userRegister.jwtLoginByPhoneNumber(payload);
        Cookie cookie = new Cookie("token", loginResponse.getResult());
        response.addCookie(cookie);
        return loginResponse;
    }

    @GetMapping("/my")
    public String myBooks() {
        return "my";
    }

    @GetMapping("/profile")
    public String profile(Model model, Authentication authentication) {
        model.addAttribute("currentUser", userRegister.getCurrentUser(authentication));
        return "profile";
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ApiResponse<User>> badCredentialsExceptionHandler(BadCredentialsException exception) {
        return  new ResponseEntity<>(new ApiResponse<>(
                HttpStatus.NOT_FOUND,
                "login or password not found",
                exception
        ), HttpStatus.NOT_FOUND);
    }
}
