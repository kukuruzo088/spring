package com.example.MyBookShopApp.security.repository;

import com.example.MyBookShopApp.enitity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookstoreUserRepository extends JpaRepository<User, Long>
{
    User findBookstoreUserByEmail(String email);

    User findBookstoreUserByPhone(String phone);
}
