package com.example.MyBookShopApp.service;

import com.example.MyBookShopApp.enitity.book.Author;
import com.example.MyBookShopApp.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class AuthorService
{
    private final AuthorRepository repository;

    @Autowired
    public AuthorService(AuthorRepository repository) {
        this.repository = repository;
    }

    public Map<String, List<Author>> getAuthorsAlphabet() {
        List<Author> authors = repository.findAll();
        return authors.stream().collect(Collectors.groupingBy((Author author) -> author.getName().substring(0,1)));
    }

    public Author findBySlug(String slug)
    {
        return repository.findAuthorBySlug(slug);
    }
}
