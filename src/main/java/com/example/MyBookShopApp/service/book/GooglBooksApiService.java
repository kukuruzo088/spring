package com.example.MyBookShopApp.service.book;

import com.example.MyBookShopApp.data.google.api.books.Root;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GooglBooksApiService
{
    @Value("${google.api.key}")
    private String googleApiKey;

    @Value("${google.api.request.url}")
    private String requestUrl;

    private final RestTemplate restTemplate;

    public GooglBooksApiService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Root getPageOfSearchResultBooks(String searchWord, Integer offset, Integer limit) {
        String url = requestUrl +
                "q=" + searchWord +
                "&key=" + googleApiKey +
                "&filter=paid-ebooks" +
                "&startIndex=" + offset +
                "&maxResults=" + limit;
        return (Root) send(url, Root.class).getBody();
    }

    private <T> ResponseEntity<T> send(String url, Class<T> entityClass) {
        return restTemplate.getForEntity(url, entityClass);
    }
}
