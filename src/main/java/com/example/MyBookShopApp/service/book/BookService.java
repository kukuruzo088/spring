package com.example.MyBookShopApp.service.book;

import com.example.MyBookShopApp.controller.book.BookRestApiController;
import com.example.MyBookShopApp.data.google.api.books.Item;
import com.example.MyBookShopApp.data.google.api.books.Root;
import com.example.MyBookShopApp.data.google.api.books.SaleInfo;
import com.example.MyBookShopApp.data.google.api.books.VolumeInfo;
import com.example.MyBookShopApp.enitity.book.Author;
import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.enitity.book.BookRating;
import com.example.MyBookShopApp.enitity.book.BookReview;
import com.example.MyBookShopApp.enitity.user.User;
import com.example.MyBookShopApp.exeption.BookstoreApiWrongParameterException;
import com.example.MyBookShopApp.repository.BookRatingRepository;
import com.example.MyBookShopApp.repository.BookRepository;
import com.example.MyBookShopApp.repository.BookReviewRepository;
import com.example.MyBookShopApp.security.service.BookstoreUserRegister;
import com.example.MyBookShopApp.service.book.action.BookAction;
import com.example.MyBookShopApp.service.factory.BookActionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Link;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Service
public class BookService
{
    private final BookRepository repository;
    private final BookRatingRepository ratingRepository;
    private final BookReviewRepository reviewRepository;
    private final BookstoreUserRegister userRegister;
    private final GooglBooksApiService googlBooksApiService;

    @Autowired
    public BookService(
            BookRepository repository,
            BookRatingRepository ratingRepository,
            BookReviewRepository reviewRepository,
            BookstoreUserRegister userRegister,
            GooglBooksApiService googlBooksApiService) {
        this.repository = repository;
        this.ratingRepository = ratingRepository;
        this.reviewRepository = reviewRepository;
        this.userRegister = userRegister;
        this.googlBooksApiService = googlBooksApiService;
    }

    public List<Book> getAllData() {
        return repository.findAll();
    }

    public List<Book> getBookByAuthor(String authorName) {
        return repository.findBooksByAuthorsNameContaining(authorName);
    }

    public List<Book> getBooksByTitle(String bookTitle) throws BookstoreApiWrongParameterException {
        if(bookTitle.length() <= 1) {
            throw new BookstoreApiWrongParameterException("Wrong values passed to one or more  parameters");
        }
        List<Book> books = repository.findBooksByTitleContaining(bookTitle);
        if(books.size() <= 0) {
            throw new BookstoreApiWrongParameterException("No data found with specified parameters...");
        }
        return books;

    }

    public List<Book> getBooksWithPriceBetween(Integer min, Integer max) {
        return addLinksToBook(repository.findBooksByPriceBetween(min, max));
    }

    private List<Book> addLinksToBook(List<Book> books) {
        books.forEach(book -> {
            String slug = book.getSlug();
            List<Link> links = new ArrayList<>();
            links.add(linkTo(BookRestApiController.class)
                    .slash("changeBookStatus")
                    .slash(slug)
                    .withRel("changeBookStatus"));

            links.add(linkTo(BookRestApiController.class)
                    .slash("changeBookStatus/cart/remove")
                    .slash(slug)
                    .withRel("removeFromCart"));

            links.add(linkTo(BookRestApiController.class)
                    .slash("review")
                    .slash(slug)
                    .withRel("addReview"));

            links.add(linkTo(BookRestApiController.class)
                    .slash("add-rating")
                    .slash(slug)
                    .withRel("addRating"));

            book.add(links);
        });
        return books;
    }

    public List<Book> getBookWithPrice(Integer price) {
        return repository.findBooksByPriceIs(price);
    }

    public List<Book> getBookWithMaxPrice() {
        return repository.getBooksWithMaxDiscount();
    }

    public List<Book> getBestsellers() {
        return repository.getBestSellers();
    }

    public Page<Book> getPageOfRecommendedBooks(Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        return repository.findAll(nextPage);
    }

    public Page<Book> getPageOfSearchResultBooks(String booksTitle, Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        return repository.findBooksByTitleContaining(booksTitle, nextPage);
    }

    public Page<Book> getPageOfRecentBooks(Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        return repository.findBooksByOrderByPubDateDesc(nextPage);
    }

    public Page<Book> getPageOfRecentBooks(Date from, Date to, Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        return repository.findBooksByPubDateBetweenOrderByPubDateDesc(from, to, nextPage);
    }

    public Page<Book> getPageOfPopularBooks(Integer offset, Integer limit) {
        Pageable nextPage = PageRequest.of(offset, limit);
        return repository.findBooksByOrderByPopularDesc(nextPage);
    }

    public Book getBookBySlug(String slug) {
        return repository.findBookBySlug(slug);
    }

    public void addToBasket(HttpServletRequest request, HttpServletResponse response, Book book) {
        BookAction bookAction = BookActionFactory.getBookAction(request, response);
        bookAction.addToBasket(book);
    }

    public void addToKept(HttpServletRequest request, HttpServletResponse response, Book book) {
        BookAction bookAction = BookActionFactory.getBookAction(request, response);
        bookAction.addToKept(book);
    }

    public List<Book> getBookInBasket(HttpServletRequest request, HttpServletResponse response) {
        BookAction bookAction = BookActionFactory.getBookAction(request, response);
        return bookAction.getBasket(repository);
    }

    public List<Book> getBookInKept(HttpServletRequest request, HttpServletResponse response) {
        BookAction bookAction = BookActionFactory.getBookAction(request, response);
        return bookAction.getKept(repository);
    }

    public BookRating addScore(Book book, BookRating rating, Authentication authentication) {
        User user = userRegister.getCurrentUser(authentication);
        rating.setBook(book);
        if(user != null && user.getId() != null) {
            rating.setUser(user);
        }
        return ratingRepository.save(rating);
    }

    public BookReview addReview(Book book, BookReview review, Authentication authentication) {
        User user = userRegister.getCurrentUser(authentication);
        review.setBook(book);
        review.setTime(new Date());
        if(user != null && user.getId() != null) {
            review.setUser(user);
        }
        return reviewRepository.save(review);
    }

    public List<Book> getPageOfGoogleBooksApiSearchResult(String searchWord, Integer offset, Integer limit) {
        Root root = googlBooksApiService.getPageOfSearchResultBooks(searchWord, offset, limit);
        List<Book> books = new ArrayList<>();
        if(root != null) {
            for(Item item: root.getItems()) {
                Book book = setBookInfoForGoogleApiBook(item);
                books.add(book);
            }
        }
        return books;
    }

    private Book setBookInfoForGoogleApiBook(Item item) {
        Book book = new Book();
        VolumeInfo bookInfo = item.getVolumeInfo();
        if(bookInfo != null) {
            List<Author> authors = new ArrayList<>();
            Author author = new Author();
            for(String authorName: bookInfo.getAuthors()) {
                author.setName(authorName);
                authors.add(author);
            }
            book.setAuthors(authors);
            book.setTitle(bookInfo.getTitle());
            book.setImage(bookInfo.getImageLinks().getThumbnail());
        }
        SaleInfo saleInfo = item.getSaleInfo();
        if(saleInfo != null) {
            book.setPrice(saleInfo.getListPrice().getAmount());
            double discount = 100 / (book.getPrice() / saleInfo.getRetailPrice().getAmount());
            if(discount < 100) {
                book.setDiscount(discount / 100);
            }
        }
        return book;
    }
}
