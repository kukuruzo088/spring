package com.example.MyBookShopApp.service.book.action;

import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.repository.BookRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface BookAction
{
    void addToBasket(Book book);

    void addToKept(Book book);

    List<Book> getBasket(BookRepository repository);

    List<Book> getKept(BookRepository repository);
}
