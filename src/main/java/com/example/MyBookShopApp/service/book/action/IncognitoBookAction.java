package com.example.MyBookShopApp.service.book.action;

import com.example.MyBookShopApp.enitity.book.Book;
import com.example.MyBookShopApp.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.StringJoiner;

public class IncognitoBookAction implements BookAction
{
    @Autowired
    private final HttpServletResponse response;
    private final Cookie[] cookies;

    public IncognitoBookAction(HttpServletRequest request, HttpServletResponse response) {
        this.response = response;
        this.cookies = request.getCookies();
    }

    private String getCookieValue(String cookieName) {
        String cookieValue = "";
        for (Cookie cookie : cookies) {
            if (cookieName.equals(cookie.getName())) {
                cookieValue = cookie.getValue();
                break;
            }
        }
        return cookieValue;
    }

    private Cookie setCookieValue(String name, String value) {
        String cartContents = getCookieValue(name);
        Cookie cookie;
        if(!cartContents.equals(value)) {
            StringJoiner stringJoiner = new StringJoiner("/");
            stringJoiner.add(cartContents).add(value);
            value = stringJoiner.toString();
        }
        cookie = new Cookie(name, value);
        cookie.setPath("/");
        return cookie;
    }

    @Override
    public void addToBasket(Book book) {
        String slug = book.getSlug();
        response.addCookie(setCookieValue("cartContents", slug));
    }

    @Override
    public void addToKept(Book book) {
        String slug = book.getSlug();
        response.addCookie(setCookieValue("keptContents", slug));
    }

    @Override
    public List<Book> getBasket(BookRepository repository) {
        String [] cookieSlugs =  cookieToArray("cartContents");
        return repository.findBooksBySlugIn(cookieSlugs);
    }

    @Override
    public List<Book> getKept(BookRepository repository) {
        String [] cookieSlugs =  cookieToArray("keptContents");
        return repository.findBooksBySlugIn(cookieSlugs);
    }

    private String[] cookieToArray(String cookieName) {
        String contents = getCookieValue(cookieName);
        contents = contents.startsWith("/") ? contents.substring(1) : contents;
        contents = contents.endsWith("/") ? contents.substring(0, contents.length() - 1) : contents;
        return contents.split("/");
    }
}
