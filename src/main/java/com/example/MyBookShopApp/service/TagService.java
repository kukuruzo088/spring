package com.example.MyBookShopApp.service;

import com.example.MyBookShopApp.enitity.book.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagService {
    private final TagRepository tagRepository;

    @Autowired
    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public List<Tag> getTagsWithBooks() {
        return tagRepository.findTagsByBooksNotNull();
    }

    public Tag getTagBySlug(String slug) {
        return tagRepository.findTagBySlug(slug);
    }
}
