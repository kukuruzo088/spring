package com.example.MyBookShopApp.service.factory;

import com.example.MyBookShopApp.service.book.action.BookAction;
import com.example.MyBookShopApp.service.book.action.IncognitoBookAction;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BookActionFactory
{
    public static BookAction getBookAction(HttpServletRequest request, HttpServletResponse response) {
        return new IncognitoBookAction(request, response);
    }
}
