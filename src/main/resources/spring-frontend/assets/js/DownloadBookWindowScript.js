$(document).ready(function() {
    $('a.btn_outline').click(function (event) {
        event.preventDefault()
        $('#myOverLay').fadeIn(297, function() {
            $('#myPopup')
                .css('display', 'block')
                .animate({opacity: 1}, 198)
        })
    })

    $('#myPopup_close, #myOverLay').click(function() {
        $('#myPopup').animate({opacity: 0}, 198, function() {
            $(this).css('display', 'none')
            $('#myOverLay').fadeOut(297)
        })
    })
})